#!/bin/bash

BACKUPS_DIR="/var/backups"
BACKUP_FILE="$BACKUPS_DIR/mysqldump-$(date +%s).sql"

# Create backups directory
mkdir -p "$BACKUPS_DIR"

# Collect all databases that DigitalOcean allows us to dump
echo "Getting database list ..."
EXPORT_TARGET=$(
  echo "SHOW DATABASES;" | \
    mysql -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" -h "$MYSQL_HOST" -P "$MYSQL_PORT" -D defaultdb | \
    grep -vE '^(Database|sys|mysql|information_schema|performance_schema|defaultdb)' | \
    tr '\n' ' ' | \
    sed -e 's/[[:space:]]*$//'
);

if [ -z "$EXPORT_TARGET" ]; then
  echo "nothing to backup" && exit 0;
fi

# Dump data to file
echo "Dumping databases to $BACKUP_FILE ..."
mysqldump \
  -u"$MYSQL_USER" \
  -p"$MYSQL_PASSWORD" \
  -h "$MYSQL_HOST" \
  -P "$MYSQL_PORT" \
  --set-gtid-purged=OFF \
  --databases $EXPORT_TARGET > "$BACKUP_FILE"

# Run backup with restic
echo "Backing up to S3 ..."
restic backup --tag mysql "$BACKUP_FILE"
