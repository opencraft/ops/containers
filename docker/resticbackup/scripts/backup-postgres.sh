#!/bin/bash

BACKUPS_DIR="/var/backups"
BACKUP_FILE="$BACKUPS_DIR/postgresdump-$(date +%s)"

# Create backups directory
mkdir -p "$BACKUPS_DIR"

# Collect all databases that DigitalOcean allows us to dump
echo "Getting database list ..."
EXPORT_TARGET=$(
  psql \
    -w -t \
    -U "$POSTGRES_USER" \
    -h "$POSTGRES_HOST" \
    -p "$POSTGRES_PORT" \
    -d "defaultdb" \
    -c 'SELECT datname FROM pg_database WHERE datistemplate = false;' | \
  grep -vE '^\ (_dodb)' | \
  sed -e 's/^[[:space:]]*//'
)

if [ -z "$EXPORT_TARGET" ]; then
  echo "nothing to backup" && exit 0;
fi

# Dump data to file
for db in $EXPORT_TARGET; do
  db_backup_file="$BACKUP_FILE-$db.sql"
  echo "Dumping database $db to $db_backup_file ..."
  pg_dump -U "$POSTGRES_USER" -h "$POSTGRES_HOST" -p "$POSTGRES_PORT" "$db" > "$db_backup_file"
done

BACKUP_TARGETS=""
for db in $EXPORT_TARGET; do
  BACKUP_TARGETS="$BACKUP_TARGETS $BACKUP_FILE-$db.sql"
done

# Run backup with restic
echo "Backing up to S3 ..."
restic backup --tag postgres $BACKUP_TARGETS
